# node-ums
Set of authentication and user management endpoints for a express/couchdb based app

# Installation 
`npm install https://gitlab.com/musamusa/node-ums`  

# Using the library
`import ums from 'node-ums'`  
`const result = ums(app, mountAt, appParams);`  
`app` is the express app instance, look at `src/index.js` for details   
`mountAt` is path to mount app at. default value is `'/ums'`    
`appParams` is ums specific params  
`result` is composed of `authService`, `bootstrap`, and `emailService`  
`authservice` for implementing authorization. you can use `authService.isAuthenticated` to make sure a user is authenticated, `authService.authorized` to implement authorization and `authService.permit` for both of them
`bootstrap` for creating default ums specific docs    
`emailService` for sending out emails  
for more info on how things work, look at the codes at `src/services` folder for now.

# test
`npm run test`
