import chai from 'chai';
import chaiHttp from 'chai-http';
import app from './server';

const { bootstrap, server, auth } = app;
const expect = chai.expect;
const token1 = auth.signToken('org.couchdb.user:ums_user');
const token2 = auth.signToken('org.couchdb.user:ums_admin');

chai.use(chaiHttp);

before((done) => {
  bootstrap(() => {
    done();
  });
});

describe(' User Authorization', () => {
  const notPermitted = { username: 'ums_user', password: 'ums_user' };
  const permitted = { username: 'ums_admin', password: 'ums_admin' };
  it('should return 403 error because user is not authorized to access /users/all', (done) => {
    chai.request(server)
    .get('/ums/users/all')
    .send(notPermitted)
    .set('Authorization', `Bearer ${token1}`)
    .end((err, res) => {
      expect(res).to.have.status(403);
      done();
    });
  });
  it('should return 200  because user is authorized to access /users/all', (done) => {
    chai.request(server)
    .get('/ums/users/all')
    .send(permitted)
    .set('Authorization', `Bearer ${token2}`)
    .end((err, res) => {
      expect(res).to.have.status(200);
      done();
    });
  });
});
