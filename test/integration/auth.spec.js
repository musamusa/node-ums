import chai from 'chai';
import chaiHttp from 'chai-http';
import app from './server';

const { bootstrap, server } = app;
const expect = chai.expect;

chai.use(chaiHttp);

before((done) => {
  bootstrap(() => {
    done();
  });
});
describe('User Login', () => {
  describe('Failed User login', () => {
    const fakeUser = { username: 'fake', password: 'fake' };
    it('should return error for wrong cred login attempt', (done) => {
      chai.request(server)
      .post('/ums/auth/')
      .send(fakeUser)
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.body).to.be.a('object');
        expect(res.body).to.have.property('message');
        expect(res.body).to.have.property('message').to.eql('Invalid username or password.');
        done();
      });
    });
  });

  describe('Successful User loggin', () => {
    const existingUser = { username: 'ums_admin', password: 'ums_admin' };
    let token;
    it('should return JWT on successful login', (done) => {
      chai.request(server)
      .post('/ums/auth/')
      .send(existingUser)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.a('object');
        expect(res.body).to.have.property('token');
        token = res.body.token;
        done();
      });
    });
    it('should get currently loggedin user', (done) => {
      chai.request(server)
      .get('/ums/users/me')
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name');
        expect(res.body.name).to.eql(existingUser.username);
        done();
      });
    });
  });
});
