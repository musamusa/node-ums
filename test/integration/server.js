import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import logger from 'morgan';
import ums from './../../src/index';

const app = express();
app.use(logger('dev'));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

const result = ums(app, undefined, { dbUrl: 'http://127.0.0.1:5999', appDBName: 'ums', secret: 'secret' });
const obj = { server: app, bootstrap: result.bootstrap, auth: result.authService };
if (!module.parent) {
  app.listen(3000, () => {
    console.log('started app on port 3000'); // eslint-disable-line no-console
  });
}

export default obj;

