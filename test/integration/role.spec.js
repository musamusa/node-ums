import chai from 'chai';
import chaiHttp from 'chai-http';
import app from './server';

const { bootstrap, server, auth } = app;
const expect = chai.expect;
const token = auth.signToken('org.couchdb.user:ums_admin');

chai.use(chaiHttp);

before((done) => {
  bootstrap(() => {
    done();
  });
});

describe('Roles Controller', () => {
  describe('Get All roles', () => {
    it('should return all roles', (done) => {
      chai.request(server)
      .get('/ums/roles')
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.a('array');
        expect(res.body).to.have.lengthOf(2);
        done();
      });
    });
  });

  describe('Create/Edit role', () => {
    it('should not create a role with invalid role name', (done) => {
      const role = { name: '', permissions: ['ums_permission_customers'] };
      chai.request(server)
      .post('/ums/roles/new')
      .set('Authorization', `Bearer ${token}`)
      .send(role)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('Role name is invalid.');
        done();
      });
    });
    it('should not create a role that exists already', (done) => {
      const role = { name: 'Admin', permissions: ['ums_permission_customers'] };
      chai.request(server)
      .post('/ums/roles/new')
      .set('Authorization', `Bearer ${token}`)
      .send(role)
      .end((err, res) => {
        expect(res).to.have.status(409);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql(`Role: ${role.name} exists.`);
        done();
      });
    });

    it('should create a role', (done) => {
      const role = { name: 'Customers', permissions: ['ums_permission_customers'] };
      chai.request(server)
      .post('/ums/roles/new')
      .set('Authorization', `Bearer ${token}`)
      .send(role)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name');
        expect(res.body).to.have.property('permissions');
        expect(res.body.permissions).to.be.a('array');
        expect(res.body.permissions).to.have.lengthOf(1);
        expect(res.body).to.have.property('_id');
        expect(res.body._id).to.eql('ums_role_customers');
        done();
      });
    });

    it('should not edit a role that does not exist', (done) => {
      const role = { _id: 'ums_role_fakerole', name: 'FakeRole', permissions: [] };
      chai.request(server)
      .post(`/ums/roles/edit/${role._id}`)
      .set('Authorization', `Bearer ${token}`)
      .send(role)
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql(`Role ${role._id} does not exist.`);
        done();
      });
    });

    it('should not be able to edit default user role', (done) => {
      const role = { _id: 'ums_role_user',
        name: 'Edit Default Role',
        permissions: ['ums_permission_customers', 'ums_permission_users'] };
      chai.request(server)
      .post(`/ums/roles/edit/${role._id}`)
      .set('Authorization', `Bearer ${token}`)
      .send(role)
      .end((err, res) => {
        expect(res).to.have.status(403);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('Can not edit default user role.');
        done();
      });
    });

    it('should edit a user role', (done) => {
      const role = { _id: 'ums_role_admin',
        name: 'Administrators',
        permissions: ['ums_permission_roles', 'ums_permission_customers', 'ums_permission_users'] };
      chai.request(server)
      .post(`/ums/roles/edit/${role._id}`)
      .set('Authorization', `Bearer ${token}`)
      .send(role)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name');
        expect(res.body.name).to.eql(role.name);
        expect(res.body).to.have.property('permissions');
        expect(res.body.permissions).to.be.a('array');
        expect(res.body.permissions).to.have.lengthOf(3);
        done();
      });
    });
    it('should get an existing role', (done) => {
      const name = 'admin';
      chai.request(server)
      .get(`/ums/roles/get/${name}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name');
        expect(res.body.name).to.eql('Administrators');
        expect(res.body).to.have.property('permissions');
        expect(res.body.permissions).to.be.a('array');
        done();
      });
    });
    it('should return error for non existing role', (done) => {
      const name = 'fakeRole';
      chai.request(server)
      .get(`/ums/roles/get/${name}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res).to.have.status(404);
        done();
      });
    });
  });
});
