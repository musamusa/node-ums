import chai from 'chai';
import chaiHttp from 'chai-http';
import app from './server';

const { bootstrap, server, auth } = app;
const expect = chai.expect;
const token = auth.signToken('org.couchdb.user:ums_admin');

chai.use(chaiHttp);

before((done) => {
  bootstrap(() => {
    done();
  });
});

describe('Permission Controller', () => {
  describe('Get All Permissions', () => {
    it('should return all roles', (done) => {
      chai.request(server)
      .get('/ums/permissions')
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.a('array');
        expect(res.body).to.have.lengthOf(10);
        done();
      });
    });

    it('should return all permissions grouped', (done) => {
      chai.request(server)
      .get('/ums/permissions?group=true')
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.a('array');
        expect(res.body).to.have.lengthOf(2);
        done();
      });
    });
  });
});
