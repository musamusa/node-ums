import chai from 'chai';
import chaiHttp from 'chai-http';
import app from './server';

const { bootstrap, server, auth } = app;
const expect = chai.expect;
const token = auth.signToken('org.couchdb.user:ums_admin');

chai.use(chaiHttp);

before((done) => {
  bootstrap(() => {
    done();
  });
});

describe('Users Controller', () => {
  describe('Get All Users', () => {
    it('should return all users', (done) => {
      chai.request(server)
        .get('/ums/users/all')
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('array');
          expect(res.body).to.have.lengthOf(2);
          done();
        });
    });
    it('should return all users by role', (done) => {
      chai.request(server)
        .get('/ums/users/all?role=admin')
        .set('Authorization', `Bearer ${token}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('array');
          expect(res.body).to.have.lengthOf(1);
          done();
        });
    });
  });

  describe('Should Create A New User', () => {
    let user;
    it('should create user', (done) => {
      user = { name: 'user1', password: 'pass' };
      chai.request(server)
      .post('/ums/users/new')
      .set('Authorization', `Bearer ${token}`)
      .send(user)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name');
        expect(res.body).to.have.property('roles');
        expect(res.body.roles).to.be.a('array');
        expect(res.body).to.have.property('accessLevel');
        expect(res.body).to.have.property('disabled');
        expect(res.body).to.have.property('firstName');
        expect(res.body).to.have.property('lastName');
        done();
      });
    });
    it('should not create user that exists already', (done) => {
      user = { name: 'user1', password: 'pass' };
      chai.request(server)
      .post('/ums/users/new')
      .set('Authorization', `Bearer ${token}`)
      .send(user)
      .end((err, res) => {
        expect(res).to.have.status(409);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('User exists already.');
        done();
      });
    });

    it('should not create user(with different letter case) that exists already', (done) => {
      user = { name: 'User1', password: 'pass' };
      chai.request(server)
      .post('/ums/users/new')
      .set('Authorization', `Bearer ${token}`)
      .send(user)
      .end((err, res) => {
        expect(res).to.have.status(409);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('User exists already.');
        done();
      });
    });

    it('should not create a user with invalid username', (done) => {
      user = { name: '', password: 'pass' };
      chai.request(server)
      .post('/ums/users/new')
      .set('Authorization', `Bearer ${token}`)
      .send(user)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('Username is invalid.');
        done();
      });
    });
    it('should get an existing user', (done) => {
      const name = 'user1';
      chai.request(server)
      .get(`/ums/users/get/${name}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name');
        expect(res.body.name).to.eql(name);
        expect(res.body).to.have.property('roles');
        expect(res.body.roles).to.be.a('array');
        expect(res.body).to.have.property('accessLevel');
        expect(res.body).to.have.property('disabled');
        expect(res.body).to.have.property('firstName');
        expect(res.body).to.have.property('lastName');
        done();
      });
    });
    it('should return error for non existing user', (done) => {
      const name = 'user does not exist';
      chai.request(server)
      .get(`/ums/users/get/${name}`)
      .set('Authorization', `Bearer ${token}`)
      .end((err, res) => {
        expect(res).to.have.status(404);
        done();
      });
    });
  });

  describe('Update User', () => {
    let user;
    it('should edit a user', (done) => {
      const name = 'user1';
      const params = { accessLevel: ['all'], firstName: 'firstName', roles: ['ums_role_user', 'ums_role_admin'] };
      chai.request(server)
      .post(`/ums/users/edit/${name}`)
      .set('Authorization', `Bearer ${token}`)
      .send(params)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.name).to.eql(name);
        expect(res.body.roles.length).to.eql(2);
        expect(res.body.accessLevel.length).to.eql(1);
        expect(res.body.firstName).to.eql(params.firstName);
        done();
      });
    });
    it('Another user should not be able to change another users password', (done) => {
      const params = { name: 'user1', oldPassword: 'pass', newPassword: 'new pass' };
      chai.request(server)
      .post('/ums/users/update-password')
      .set('Authorization', `Bearer ${token}`) // using another users token
      .send(params)
      .end((err, res) => {
        expect(res).to.have.status(401);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('Unauthorized, cannot change another users password');
        done();
      });
    });
    it('should not change users password when old password is wrong', (done) => {
      const params = { name: 'user1', oldPassword: 'wrong password', newPassword: 'new pass' };
      const token1 = auth.signToken('org.couchdb.user:user1');
      chai.request(server)
      .post('/ums/users/update-password')
      .set('Authorization', `Bearer ${token1}`)
      .send(params)
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('Wrong password.');
        done();
      });
    });
    it('should change users password when old password is correct', (done) => {
      const params = { name: 'user1', oldPassword: 'pass', newPassword: 'new pass' };
      const token1 = auth.signToken('org.couchdb.user:user1');
      chai.request(server)
      .post('/ums/users/update-password')
      .set('Authorization', `Bearer ${token1}`)
      .send(params)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name');
        expect(res.body.name).to.eql(params.name);
        done();
      });
    });
    it('should verify that password changed by logging in with old credentials', (done) => {
      user = { username: 'user1', password: 'pass' };
      chai.request(server)
      .post('/ums/auth/')
      .send(user)
      .end((err, res) => {
        expect(res).to.have.status(401);
        expect(res.body).to.be.a('object');
        expect(res.body).to.have.property('message');
        expect(res.body).to.have.property('message').to.eql('Invalid username or password.');
        done();
      });
    });

    it('should verify that password changed by logging in with new credentials', (done) => {
      user = { username: 'user1', password: 'new pass' };
      chai.request(server)
      .post('/ums/auth/')
      .send(user)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.be.a('object');
        expect(res.body).to.have.property('token');
        done();
      });
    });

    it('Non Admin user should not be able to call admin reset password for a user', (done) => {
      const params = { id: 'user1', password: 'new pass' };
      const token1 = auth.signToken('org.couchdb.user:ums_user');
      chai.request(server)
      .post('/ums/users/update-password-admin')
      .set('Authorization', `Bearer ${token1}`) // using another users token
      .send(params)
      .end((err, res) => {
        expect(res).to.have.status(403);
        expect(res.body).to.have.property('message');
        expect(res.body.message).to.eql('Forbidden');
        done();
      });
    });

    it('Admin user should be able to call admin reset password for a user', (done) => {
      const params = { id: 'user1', password: 'new pass' };
      chai.request(server)
      .post('/ums/users/update-password-admin')
      .set('Authorization', `Bearer ${token}`)
      .send(params)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body).to.have.property('name');
        expect(res.body.name).to.eql(params.id);
        done();
      });
    });
  });

  describe('Manage User status', () => {
    let user;
    it('disable a user', (done) => {
      const name = 'user1';
      const params = { disabled: true };
      chai.request(server)
      .post(`/ums/users/edit/${name}`)
      .set('Authorization', `Bearer ${token}`)
      .send(params)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.name).to.eql(name);
        expect(res.body.disabled).to.eql(true);
        done();
      });
    });
    it('disabled user should not be able to login', (done) => {
      user = { username: 'user1', password: 'pass' };
      chai.request(server)
      .post('/ums/auth/')
      .send(user)
      .end((err, res) => {
        expect(res).to.have.status(401);
        expect(res.body).to.be.a('object');
        expect(res.body).to.have.property('message');
        expect(res.body).to.have.property('error').to.eql('User disabled.');
        done();
      });
    });
  });

  describe('Reset users password', () => {
    it('Get user reset token', (done) => {
      const resetToken = 'reset-token-1';
      chai.request(server)
      .get(`/ums/users/reset-token/${resetToken}`)
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body._id).to.eql(resetToken);
        expect(res.body.active).to.eql(true);
        done();
      });
    });

    it('Request password reset for a user with pending reset request should respond with old token ', (done) => {
      const resetToken = 'reset-token-1';
      const user = 'ums_user';
      chai.request(server)
      .post('/ums/users/request-password-reset')
      .send({ id: user })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.id).to.eql(resetToken);
        done();
      });
    });

    it('Request password reset for a user without a pending reset request should respond with a fresh token to user', (done) => {
      const resetToken = 'reset-token-2';
      const user = 'ums_admin';
      chai.request(server)
        .post('/ums/users/request-password-reset')
        .send({ id: user })
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body.id).to.not.eql(resetToken);
          done();
        });
    });

    it('Request password reset for a non existing user should return 404', (done) => {
      const user = 'fakeUser';
      chai.request(server)
      .post('/ums/users/request-password-reset')
      .send({ id: user })
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql(`User ${user} does not exists in the system.`);
        done();
      });
    });

    it('Reset password should return updated user successfully for a valid unused token', (done) => {
      const resetToken = 'reset-token-1';
      const password = 'new_password';
      const user = 'ums_user';
      chai.request(server)
      .post('/ums/users/reset-password')
      .send({ password, token: resetToken })
      .end((err, res) => {
        expect(res).to.have.status(200);
        expect(res.body.name).to.eql(user);
        done();
      });
    });

    it('Reset password should return expired for an expired token', (done) => {
      const resetToken = 'reset-token-1';
      const password = 'new_password';
      chai.request(server)
      .post('/ums/users/reset-password')
      .send({ password, token: resetToken })
      .end((err, res) => {
        expect(res).to.have.status(400);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('Token expired.');
        done();
      });
    });

    it('Reset password should return invalid token for a token not found', (done) => {
      const resetToken = 'invalid-token';
      const password = 'new_password';
      chai.request(server)
      .post('/ums/users/reset-password')
      .send({ password, token: resetToken })
      .end((err, res) => {
        expect(res).to.have.status(404);
        expect(res.body).to.have.property('error');
        expect(res.body.error).to.eql('Invalid token.');
        done();
      });
    });
  });

  describe('User Admin Level Restrictions', () => {
    it('should return only users in user\'s state', (done) => {
      const token2 = auth.signToken('org.couchdb.user:ums_admin');
      chai.request(server)
        .get('/ums/users/all')
        .set('Authorization', `Bearer ${token2}`)
        .end((err, res) => {
          expect(res).to.have.status(200);
          expect(res.body).to.be.a('array');
          expect(res.body).to.have.lengthOf(3);
          done();
        });
    });
  });
});
