export const UMS_DEFAULT_USER_ROLE = 'ums_role_user';
export const UMS_ROLE_PREFIX = 'ums_role_';
export const UMS_ROLE_DOC_TYPE = 'user-role';
export const UMS_USER_DOC_TYPE = 'user';
export const UMS_USER_PREFIX = 'org.couchdb.user:';
export const UMS_PERMISSION_PREFIX = 'ums_permission_';

