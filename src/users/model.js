import _ from 'lodash';
import crypto from 'crypto';
import { generate } from 'random-password-generator';
import TokenGenerator from 'uuid-token-generator';
import PouchDB from 'pouchdb';
import { UnauthorizedError, BadRequestError, ConflictError, NotFoundError, ForbiddenError } from '../helpers/errors';
import emailConfig from '../services/email/config';
import dbService from '../services/db/index';
import dbViews from '../services/db/constants';
import { UMS_USER_PREFIX, UMS_DEFAULT_USER_ROLE, UMS_ROLE_PREFIX, UMS_USER_DOC_TYPE, UMS_ROLE_DOC_TYPE } from '../helpers/constants';


const verifyPassword = (password, userDoc, cb) => {
  crypto.pbkdf2(password, userDoc.salt, userDoc.iterations, userDoc.derived_key.length / 2, 'sha1',
      (error, derivedKey) => {
        if (!error && userDoc.derived_key === derivedKey.toString('hex')) {
          return cb(null, userDoc);
        }
        return cb(new UnauthorizedError('Invalid username or password.'));
      });
};

function getChildrenPermissions(root, permissions) {
  const parent = root;
  const children = permissions.filter(permission => permission['parent-permission'] === parent._id);
  if (children.length) {
    parent.children = children;
    children.forEach(permission => getChildrenPermissions(permission, permissions));
  }
  return parent;
}

function groupPermissions(permissions) {
  return permissions.filter(permission => !permission['parent-permission'])
  .map(root => getChildrenPermissions(root, permissions));
}

const fixPrefix = (prefix, id) => {
  let _id = id.toLowerCase();
  if (!_id.startsWith(prefix)) {
    _id = `${prefix}${id}`;
  }
  return _id;
};

const prefixRoleId = fixPrefix.bind(null, UMS_ROLE_PREFIX);


export default function user(appName,
  dashboardUrl,
  dbUrl,
  usersDBName,
  appDBName,
  emailService, authPaths, sendEmails) {
  const usersDB = new PouchDB(`${dbUrl}/${usersDBName}`);
  const appDB = new PouchDB(`${dbUrl}/${appDBName}`);

  return {
    findById(id, cb) {
      usersDB.get(id, (err, userDoc) => {
        cb(err, userDoc);
      });
    },

    findUser(name, cb) {
      const Id = `org.couchdb.user:${name}`;
      this.findById(Id, cb);
    },

    buildUser(u) {
      return _.omit(u, ['type', 'password_scheme', 'derived_key', 'salt', 'iterations']);
    },

    authenticate(name, password, cb) {
      this.findUser(name, (err, userDoc) => {
        if (err && err.error !== 'not_found') {
          return cb(new Error('Internal error.'));
        }

        if (!userDoc || (err && err.error === 'not_found')) {
          return cb(new NotFoundError('Invalid username or password.'));
        }

        if (userDoc.disabled) {
          return cb(new UnauthorizedError('Access Denied.', 'User disabled.'));
        }

        if (!userDoc.password_scheme || userDoc.password_scheme !== 'pbkdf2') {
          return cb(new BadRequestError(`Password scheme not supported: ${userDoc.password_scheme}`));
        }

        if (!userDoc.salt || !userDoc.iterations || !userDoc.derived_key) {
          return cb(new BadRequestError('Invalid user authentication data.'));
        }

        return verifyPassword(password, userDoc, (error, data) => {
          if (error) {
            return cb(error);
          }
          return cb(error, data);
        });
      });
    },

    createUser(userData, cb) {
      const onSendPasswordEmail = (callback, doc, err) => {
        if (err) {
          return callback(new BadRequestError('Error sending password to user, try again later'));
        }
        return callback(err, this.buildUser(doc));
      };

      const onSaveUser = (data, error, res) => {
        if (error) {
          return cb(error);
        }
        return this.findById(res.id, (err, doc) => {
          if (err) {
            return cb(err);
          }
          if (sendEmails) {
            const subject = `Welcome to ${appName}`;
            const message = _.template(emailConfig.newUserEmailTemplate)(
                  { dashboardUrl, username: doc.name, password: data.password, user: doc.firstName || 'User', ...authPaths });
            return emailService.sendEmail({ emails: doc.name,
              subject,
              message,
              cb: onSendPasswordEmail.bind(null, cb, doc),
              emailSender: emailConfig.emailSender }
            );
          }
          return cb(err, this.buildUser(doc));
        });
      };

      if (!userData.name) {
        return cb(new BadRequestError('Username is invalid.'));
      }

      const data = userData;
      data.name = data.name.toLowerCase();
      return this.findUser(data.name, (err, u) => {
        if (u && !err) {
          return cb(new ConflictError('User exists already.'));
        }
        data.type = UMS_USER_DOC_TYPE;
        data.disabled = !!data.disabled;
        data._id = `${UMS_USER_PREFIX}${data.name}`;
        data.firstName = data.firstName || '';
        data.lastName = data.lastName || '';
        if (!data.roles || !data.roles.length) {
          data.roles = [UMS_DEFAULT_USER_ROLE];
        }

        if (!data.accessLevel) {
          data.accessLevel = ['all'];
        }
        data.password = data.password || generate();
        return usersDB.put(data, onSaveUser.bind(null, data));
      });
    },

    all(state, role, cb) {
      usersDB.allDocs({ include_docs: true, startkey: UMS_USER_PREFIX }, (err, users) => {
        if (err) {
          return cb(err);
        }
        let userDocs = users.rows
          .map(u => u.doc)
          .map(u => this.buildUser(u));
        if (role) {
          const roleId = prefixRoleId(role);
          userDocs = userDocs.filter(doc => doc.roles && doc.roles.includes(roleId));
        }
        return cb(err, userDocs);
      });
    },

    get(id, cb) {
      this.findUser(id, (err, userDoc) => {
        if (err) {
          return cb(err);
        }
        return cb(err, this.buildUser(userDoc));
      });
    },

    updateMyPassword(id, oldPassword, newPassword, loggedInUser, cb) {
      if (!loggedInUser || loggedInUser.name !== id) {
        return cb(new UnauthorizedError('Unauthorized, cannot change another users password'));
      }
      return this.findUser(id, (err, userDoc) => {
        if (err) {
          return cb(new NotFoundError(`User ${id} does not exists in the system.`));
        }
        return verifyPassword(oldPassword, userDoc, (error) => {
          if (error) {
            return cb(new BadRequestError('Wrong password.'));
          }
          return this.changeUserPassword(userDoc, newPassword, cb);
        });
      });
    },

    editUser(id, data, cb) {
      return this.findUser(id, (err, userDoc) => {
        if (err) {
          return cb(new NotFoundError(`User ${id} does not exists in the system.`));
        }
        const params = data;
        const resetPassword = params.resetPassword;
        const newPassword = params.password;
        delete params.password; // clears password property
        delete params.resetPassword; // clear reset password
        const editedUser = Object.assign(userDoc, params);
        return usersDB.put(editedUser, (error, result) => {
          editedUser._rev = result.rev;
          if (error) {
            return cb(error);
          }
          if (resetPassword) {
            return this.changeUserPassword(editedUser, newPassword || generate(), cb, true);
          }
          return cb(error, this.buildUser(editedUser));
        });
      });
    },

    requestPasswordReset(id, cb) {
      const onSendResetEmail = (callback, res, err) => {
        if (err) {
          return callback(new BadRequestError('Error sending reset email to user, try again later'));
        }
        return callback(err, res);
      };

      const onSaveToken = (data, userDoc, error, res) => {
        if (error) {
          return cb(error);
        }
        if (sendEmails) {
          const subject = 'You have requested a password reset';
          const message = _.template(emailConfig.resetUserEmailTemplate)({ dashboardUrl, token: data._id, user: userDoc.firstName || 'User', ...authPaths });
          return emailService.sendEmail({ emails: id,
            subject,
            message,
            cb: onSendResetEmail.bind(null, cb, res),
            emailSender: emailConfig.emailSender }
          );
        }
        return cb(error, res);
      };

      this.findUser(id, (userError, userDoc) => {
        if (userError) {
          return cb(new NotFoundError(`User ${id} does not exists in the system.`));
        }
        return dbService.getView(appDB, dbViews.resetTokens, `active-${id}`)
          .then((rows) => {
            let data;
            if (rows.length) {
              data = rows[0].doc;
            } else {
              const tokgen = new TokenGenerator();
              const token = tokgen.generate();
              data = { _id: token, user: id, active: true, doc_type: 'reset-token', createdOn: new Date() };
            }
            return appDB.put(data, onSaveToken.bind(null, data, userDoc));
          });
      });
    },

    resetUserPassword(token, password, cb) {
      this.getResetToken(token, (err, data) => {
        if (err) {
          return cb(err);
        }
        return this.findUser(data.user, (userError, userDoc) => {
          if (userError) {
            return cb(new NotFoundError('User does not exists in the system.'));
          }
          const resetToken = data;
          resetToken.active = false;
          resetToken.usedOn = new Date();
          return appDB.put(resetToken, (error) => {
            if (error) {
              return cb(error);
            }
            return this.changeUserPassword(userDoc, password, cb);
          });
        });
      });
    },

    updateUserPassword(id, newPassword, cb) {
      return this.findUser(id, (err, userDoc) => {
        if (err) {
          return cb(new NotFoundError(`User ${id} does not exists in the system.`));
        }
        return this.changeUserPassword(userDoc, newPassword, cb, true);
      });
    },

    onUpdateUser(cb, userDoc, sendPassword, err) {
      const onSendConfirmationEmail = (callback, doc, error) => {
        if (error) {
          return callback(new BadRequestError('Error sending password update email, try again later'));
        }
        return callback(error, this.buildUser(doc));
      };

      if (err) {
        return cb(err);
      }
      if (sendEmails) {
        const subject = sendPassword ? 'Your Password has been reset' : 'Your Password Changed';
        const message = _.template(emailConfig.passwordUpdateEmail(sendPassword))(
            { dashboardUrl, user: userDoc.firstName || 'User', password: userDoc.password, ...authPaths });
        return emailService.sendEmail({ emails: userDoc.name,
          subject,
          message,
          cb: onSendConfirmationEmail.bind(this, cb, userDoc),
          emailSender: emailConfig.emailSender });
      }
      return cb(err, this.buildUser(userDoc));
    },

    changeUserPassword(userDoc, newPassword, cb, sendPassword) {
      const editedUser = userDoc;
      editedUser.password = newPassword;
      return usersDB.put(editedUser, this.onUpdateUser.bind(this, cb, editedUser, sendPassword));
    },

    getResetToken(token, cb) {
      return appDB.get(token, (err, doc) => {
        if (err) {
          return cb(new NotFoundError('Invalid token.'));
        }
        if (!doc.active) {
          return cb(new BadRequestError('Token expired.'));
        }
        return cb(err, doc);
      });
    },

    allRoles() {
      return dbService.getView(appDB, dbViews.roles)
      .then(res => res.map(role => role.doc));
    },

    getPermissionsInRole(role) {
      return dbService.getView(appDB, dbViews.roles, role)
      .then((docs) => {
        if (docs[0] && docs[0].doc) {
          return docs[0].doc.permissions;
        }
        return [];
      })
      .catch(() => []);
    },

    allPermissions(group) {
      return dbService.getView(appDB, dbViews.permissions)
      .then(res => res.map(role => role.doc))
      .then((docs) => {
        if (group) {
          return groupPermissions(docs);
        }
        return docs;
      });
    },

    createRole(role, cb) {
      const data = role;
      if (!data.name) {
        return cb(new BadRequestError('Role name is invalid.'));
      }
      data.name = data.name.trim();
      let id = data.name.toLowerCase();
      id = id.replace(/\s+/g, '-');
      const _id = `${UMS_ROLE_PREFIX}${id}`;
      return appDB.get(_id, (err, doc) => {
        if (doc && !err) {
          return cb(new ConflictError(`Role: ${data.name} exists.`));
        }
        data._id = _id;
        data.permissions = data.permissions || [];
        data.doc_type = UMS_ROLE_DOC_TYPE;

        return appDB.put(data, (error, result) => {
          if (error) {
            return cb(error);
          }
          return appDB.get(result.id, (err2, roleDoc) => {
            if (err2) {
              return cb(err2);
            }
            return cb(null, roleDoc);
          });
        });
      });
    },

    editRole(id, data, cb) {
      const _id = prefixRoleId(id);
      if (_id === UMS_DEFAULT_USER_ROLE) {
        return cb(new ForbiddenError('Can not edit default user role.'));
      }
      return appDB.get(_id, (err, doc) => {
        if (err) {
          return cb(new NotFoundError(`Role ${id} does not exist.`));
        }
        const editedDoc = Object.assign(doc, data);
        return appDB.put(editedDoc, (error, result) => {
          if (error) {
            return cb(error);
          }
          return appDB.get(result.id, (err2, roleDoc) => {
            if (err2) {
              return cb(err2);
            }
            return cb(null, roleDoc);
          });
        });
      });
    },

    findRole(id, cb) {
      const _id = prefixRoleId(id);
      return appDB.get(_id, (err, doc) => {
        if (err) {
          return cb(new NotFoundError(`Role ${id} does not exist.`));
        }
        return cb(err, doc);
      });
    }
  };
}
