import usersModel from './model';

const userService = usersModel('appName', 'test-url', 'test-url', '_users');

describe('users', () => {
  describe('findById', () => {
    it('should be defined', () => {
      expect(userService.findById).toBeDefined();
    });
  });
});
