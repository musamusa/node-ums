const handleResponse = (res, err, data) => {
  if (err) {
    return res.status(err.status).json({ error: err.message });
  }
  return res.status(200).json(data);
};

export default function userController(user) {
  return {
    createUser(req, res) {
      user.createUser(req.body, handleResponse.bind(null, res));
    },

    allUsers(req, res) {
      user.all(req.user.accessLevel[0], req.query.role, handleResponse.bind(null, res));
    },

    getUser(req, res) {
      user.get(req.params.id, handleResponse.bind(null, res));
    },

    getMe(req, res) {
      return res.json(user.buildUser(req.user));
    },

    updateMyPassword(req, res) {
      user.updateMyPassword(req.body.name,
        req.body.oldPassword, req.body.newPassword,
        req.user, handleResponse.bind(null, res));
    },

    editUser(req, res) {
      user.editUser(req.params.id, req.body, handleResponse.bind(null, res));
    },

    requestPasswordReset(req, res) {
      user.requestPasswordReset(req.body.id, handleResponse.bind(null, res));
    },

    resetUserPassword(req, res) {
      user.resetUserPassword(req.body.token, req.body.password, handleResponse.bind(null, res));
    },

    getResetToken(req, res) {
      user.getResetToken(req.params.id, handleResponse.bind(null, res));
    },

    updateUserPassword(req, res) {
      user.updateUserPassword(req.body.id, req.body.password, handleResponse.bind(null, res));
    },

    getRoles(req, res, next) {
      return user.allRoles()
      .then(res.json.bind(res))
      .catch(next);
    },

    getPermissions(req, res, next) {
      return user.allPermissions(req.query.group)
      .then(res.json.bind(res))
      .catch(next);
    },

    createRole(req, res) {
      user.createRole(req.body, handleResponse.bind(null, res));
    },

    editRole(req, res) {
      user.editRole(req.params.id, req.body, handleResponse.bind(null, res));
    },

    findRole(req, res) {
      user.findRole(req.params.id, handleResponse.bind(null, res));
    }
  };
}
