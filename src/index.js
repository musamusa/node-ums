/* eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
import couchBootstrap from 'couchdb-bootstrap';
import ensure from 'couchdb-ensure';
import get from 'simple-get';
import express from 'express';
import passport from 'passport';
import HTTPStatus from 'http-status';
import path from 'path';

import userController from './users/controller';
import user from './users/model';
import auth from './services/auth/auth';
import authenticate from './services/auth/index';
import setup from './services/auth/passport';
import email from './services/email/index';

const async = require('async');
const appRootDir = require('app-root-dir').get();

function push(docs, bulkDocsUrl, cb) {
  const docsJSON = JSON.stringify({ docs });

  const options = {
    url: bulkDocsUrl,
    body: docsJSON,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    }
  };

  return get.concat(options, (err, data) => {
    if (err) {
      return cb(err);
    }
    return cb(err, data.statusCode);
  });
}

function couchDBBootstrap(baseUrl, bootstrapDir, dbName = 'ums', done) {
  couchBootstrap(baseUrl,
    bootstrapDir, {
      mapDbName(name) {
        return name === '_users' ? name : dbName;
      }
    },
  (err) => {
    if (err) {
      return done(err);
    }
    return done();
  });
}

function couchDBFixtures(type, baseUrl, bulkDocsUrl, fixturesDir, done) {
  return ensure(baseUrl, (error) => {
    if (error) {
      return done(error);
    }
    const data = require(fixturesDir)(type); // eslint-disable-line global-require
    return async.each(data, (value, callback) => {
      const docs = value.docs;
      return push(docs, bulkDocsUrl, (err) => {
        if (err) {
          return callback(err);
        }
        return callback();
      });
    }, (err2) => {
      if (err2) {
        return done(err2);
      }
      return done();
    });
  });
}

export default function ums(expressApp, mountAt = '/ums',
  { dbUrl,
    appDBName,
    usersDBName = '_users',
    secret = '',
    appUrl = '',
    appName = 'LoMIS Deliver',
    sendEmails = false,
    authPaths = {
      login: '#/auth/login',
      reset: '#/auth/reset-password',
      forgot: '#/auth/forgot-password'
    }
  }) {
  const bulkDocsUrl = `${dbUrl}/${appDBName}/_bulk_docs`;
  const type = process.NODE_ENV === 'test' ? process.NODE_ENV : 'main';
  const couchDir = path.relative(appRootDir, path.join(__dirname, '..', 'couchdb'));
  const bootstrapDir = `${couchDir}/bootstrap`;
  const fixturesDir = process.NODE_ENV === 'test' ?
    `./${path.join('../', couchDir, 'fixtures')}`
    : `${path.join(couchDir.replace('node_modules/', ''), 'fixtures')}`;
  const emailService = email();
  const userService = user(appName,
    appUrl,
    dbUrl,
    usersDBName,
    appDBName,
    emailService,
    authPaths,
    sendEmails);
  const controller = userController(userService);
  const authService = auth(userService, secret);
  const router = express.Router();

  function bootstrap(done) {
    async.eachSeries([couchDBBootstrap.bind(null, dbUrl, bootstrapDir, appDBName),
      couchDBFixtures.bind(null, type, dbUrl, bulkDocsUrl, fixturesDir)],
      (fn, callback) => fn(callback),
      (err) => {
        if (err) {
          return done(err);
        }
        return done();
      });
  }

  setup(userService);
  expressApp.use(passport.initialize());
  router.post('/auth', authenticate.bind(null, authService));
  router.get('/permissions', authService.permit('roles_view'), controller.getPermissions);
  router.get('/roles', authService.permit('roles_view'), controller.getRoles);
  router.post('/roles/new', authService.permit('roles_create'), controller.createRole);
  router.post('/roles/edit/:id', authService.permit('roles_edit'), controller.editRole);
  router.get('/roles/get/:id', authService.permit('roles_view'), controller.findRole);
  router.get('/users/me', authService.isAuthenticated(), controller.getMe);
  router.post('/users/new', authService.permit('users_create'), controller.createUser);
  router.get('/users/all', authService.permit('users_view'), controller.allUsers);
  router.get('/users/get/:id', authService.permit('users_view'), controller.getUser);
  router.post('/users/edit/:id', authService.permit('users_edit'), controller.editUser);
  router.post('/users/update-password', authService.isAuthenticated(), controller.updateMyPassword);
  router.post('/users/request-password-reset', controller.requestPasswordReset);
  router.post('/users/reset-password', controller.resetUserPassword);
  router.get('/users/reset-token/:id', controller.getResetToken);
  router.post('/users/update-password-admin', authService.permit('users_manage'), controller.updateUserPassword);
  expressApp.use(mountAt, router, (err, req, res, next) => {
    err.status = err.status || HTTPStatus.INTERNAL_SERVER_ERROR; // eslint-disable-line
    res.status(err.status)
    .json({ message: err.message, error: err.error || {} });
    if (err.status > 499) console.log(err); // eslint-disable-line
    next();
  });
  return { emailService, authService, bootstrap };
}

