const fetchView = (db, ...params) => {
  const [view, key, include_docs, startkey, endkey] = params; // eslint-disable-line camelcase
  const options = { key, include_docs, startkey, endkey };
  return db.query(view, options);
};

const getView = (db, view, key, includeDocs = true) =>
  fetchView(db, view, key, includeDocs)
    .then(res => res.rows);

export default { getView };
