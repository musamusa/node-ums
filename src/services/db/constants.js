const VIEWS = {
  roles: 'user-roles/by-id',
  resetTokens: 'reset-tokens/by-state',
  permissions: 'user-permissions/all'
};

export default VIEWS;
