export default {
  newUserEmailTemplate: `Hello <%= user %>, <br/> <br/>
               Welcome to LoMIS Deliver Dashboard.<br/>
               Click <a href="<%=dashboardUrl %><%=login %>">here</a> to log in with the credentials below:<br/><br/>
               User name: <em><%=username %> </em><br/>
               Password:  <em><%=password %> </em> <br/><br/>
               You are advised to change your password immediately you login. <br/><br/>
               Thanks. <br/>
               LoMIS Deliver Team`,
  resetUserEmailTemplate: `Hello <%= user %>, <br/> <br/>
               You have recently requested a password reset on your LoMIS Deliver account.
               Please, click <a href="<%=dashboardUrl %><%=reset %>/<%= token %>">here</a> to reset your password. 
               If you did not initiate a password reset, please <a href="<%=dashboardUrl %>">login</a> to change your password and report this to your Admin<br/><br/>
               Thanks. <br/>
               LoMIS Deliver Team`,
  passwordUpdateEmail(sendPassword) {
    const start = 'Hello <%= user %>,';
    const content = sendPassword ? `Your LoMIS Deliver account password was recently reset,
        and your new password is <em><%= password %></em>.
        You are advised to change your password upon login.` : `Your password on LoMIS Deliver was recently changed.
        Ignore this message if you changed your password.
        Otherwise please <a href="<%=dashboardUrl %><%=forgot %>">reset</a>.`;
    const end = 'Thanks. <br/> LoMIS Deliver Team';
    return `${start}<br/><br/>${content}<br/><br/>${end}`;
  },
  emailSender: ''
};
