import AWS from 'aws-sdk';

AWS.config.update({ region: 'eu-west-1' });

export default function email() {
  const ses = new AWS.SES({ apiVersion: '2010-12-01' });
  const sendEmail = (options) => {
    const mailOptions = {
      Destination: {
        ToAddresses: [options.emails]
      },
      Message: {
        Body: {
          Html: {
            Data: options.message
          },
          Text: {
            Data: options.htmlMessage || `<b> ${options.message}</b>`
          }
        },
        Subject: {
          Data: options.subject
        }
      },
      Source: options.emailSender,
      ReplyToAddresses: [options.emailSender],
      ReturnPath: options.emailSender
    };
    return ses.sendEmail(mailOptions, options.cb);
  };

  return {
    sendEmail
  };
}
