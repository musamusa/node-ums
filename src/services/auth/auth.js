import Promise from 'bluebird';
import ejwt from 'express-jwt';
import jwt from 'jsonwebtoken';
import composer from 'composable-middleware';
import _ from 'lodash';
import { UMS_PERMISSION_PREFIX } from '../../helpers/constants';
import isUserAuthorized from './authorized-user';


function auth(user, secret) {
  const validateJwt = ejwt({ secret });
  return {
    isAuthenticated() {
      return composer()
      .use((req, res, next) => {
      // validate only req which has authorization headers set with jwt access token
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
          return validateJwt(req, res, next);
        }
        return res.status(401).json({ message: 'unauthorized access!' });
      })
      .use((req, res, next) => {
        user.findById(req.user._id, (err, data) => {
          if (err) return next(err);
          if (!data) return res.send(401);
          // load user permissions
          return Promise.map(data.roles, user.getPermissionsInRole)
          .then((arrays) => {
            const reduced = arrays.reduce((prev, curr) => prev.concat(curr), []);
            const permissions = _.uniq(reduced);
            data.permissions = permissions; // eslint-disable-line no-param-reassign
            req.user = data; // eslint-disable-line no-param-reassign
            return next();
          })
          .catch(next);
        });
      });
    },

    signToken(id) {
      return jwt.sign({ _id: id }, secret);
    },

    isUserAuthorized: isUserAuthorized.bind(null, UMS_PERMISSION_PREFIX),

    authorized(...requiredPermissions) {
      return (req, res, next) => {
        if (req.user && this.isUserAuthorized(requiredPermissions, req.user)) {
          next();
        } else {
          res.status(403).json({ message: 'Forbidden' });
        }
      };
    },
    permit(...permissions) {
      return composer()
        .use(this.isAuthenticated())
        .use(this.authorized(permissions));
    }
  };
}

export default auth;
