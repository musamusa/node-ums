import passport from 'passport';
import { BadRequestError } from '../../helpers/errors';

export { isUserAuthorized } from './auth';

const authenticate = (authService, req, res, next) => {
  passport.authenticate('local', (err, user) => {
    if (err) return next(err);
    if (!user) throw new BadRequestError('Invalid Request!');

    return res.json({ token: authService.signToken(user._id) });
  })(req, res, next);
};

export default authenticate;
