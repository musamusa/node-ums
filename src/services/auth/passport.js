import passport from 'passport';
import LocalPassport from 'passport-local';

const LocalStrategy = LocalPassport.Strategy;

// setup passport Strategy
const setup = (user) => {
  passport.use(new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password'
  },
  (username, password, done) => {
    user.authenticate(username.toLowerCase(), password, done);
  },
));
};

export default setup;
