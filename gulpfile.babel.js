/* eslint no-console: ["error", { allow: ["warn", "error", "log"] }] */
import del from 'del';
import gulp from 'gulp';
import runSequence from 'run-sequence';
import babelCompiler from 'babel-register';
import gulpLoadPlugins from 'gulp-load-plugins';
import yargs from 'yargs';

const eslint = require('gulp-eslint');
const gutil = require('gulp-util');
const exec = require('child_process').exec;
const spawnPouchdbServer = require('spawn-pouchdb-server');

const argv = yargs.argv;
const plugins = gulpLoadPlugins();
const paths = {
  js: ['./couchdb/**/*.json', '**/*.js', '!dist/**', '!node_modules/**'],
  nonJs: ['./package.json', './.gitignore'],
  tests: {
    integration: './test/integration/*.js',
    unit: './test/unit/*.js'
  },
  build: 'dist'
};


gulp.task('lint', () => gulp.src(['**/*.js', '!node_modules/**', '!couchdb/**', '!dist/**'])
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError()),
);

gulp.task('clean', () =>
  del(['dist/**', '!dist']),
);

gulp.task('copy', ['clean'], () =>
  gulp.src(paths.nonJs, { base: '.' })
    .pipe(plugins.newer(paths.build))
    .pipe(gulp.dest(paths.build)),
);

gulp.task('babel', ['copy'], () =>
  gulp.src([...paths.js, '!gulpfile.babel.js'], { base: '.' })
    .pipe(plugins.newer(paths.build))
    .pipe(plugins.babel({ compact: false }))
    .pipe(gulp.dest(paths.build)),
);


gulp.task('test', () => {
  const testType = argv.unit ? 'unit' : 'integration';

  return gulp.src([paths.tests[testType]], { read: false })
    .pipe(plugins.plumber())
    .pipe(plugins.mocha({
      reporter: 'spec',
      ui: 'bdd',
      recursive: true,
      compilers: {
        js: babelCompiler
      }
    }));
});

gulp.task('integration-test', ['set-test-node-env'], (done) => {
  gulp.on('stop', () => { process.exit(0); });
  gulp.on('err', () => { process.exit(1); });

  function cleanup() {
    gutil.log('removing database dir .db');
    exec('rm -r .db');
  }
  exec('mkdir -p .db', () => {
    process.on('SIGINT', cleanup);
    process.on('exit', cleanup);
  });

  spawnPouchdbServer({ port: 5999, verbose: true }, (error, server) => {
    // start an express server server, mounth routes here
    runSequence('test', () => {
      server.stop(() => {
        gutil.log('PouchDB Server stopped');
        done();
      });
    });
  });
});

gulp.task('default', () => {
  runSequence(['copy', 'babel']);
});

gulp.task('set-test-node-env', () => {
  process.NODE_ENV = 'test';
});
