const password = process.env.UMS_PASSWORD
module.exports = {
  _id: 'org.couchdb.user:ums_user',
  type: 'user',
  name: 'ums_user',
  password: password || 'ums_user',
  disabled: false,
  roles:['ums_role_user'],
  accessLevel: ['Kano'],
  firstName: 'User',
  lastName: 'User'
}
