
const password = process.env.UMS_PASSWORD
module.exports = {
  _id: 'org.couchdb.user:ums_admin',
  type: 'user',
  name: 'ums_admin',
  password: password || 'ums_admin',
  disabled: false,
  roles:['ums_role_admin'],
  accessLevel: ['all'],
  firstName: 'Admin',
  lastName: 'Admin'
}
