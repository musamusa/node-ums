function(doc) {
  if(doc.doc_type === 'reset-token') {
    emit((doc.active ? 'active' : 'not-active') + '-' + doc.user);
  }
}
