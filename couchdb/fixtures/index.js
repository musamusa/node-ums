/* eslint-env node */

const fs = require('fs');
const path = require('path');
const data = {}

function isJSON(file) {
    return file.match(/.json$/);
}

function filterTest (file) {
  return file.indexOf('.test') === - 1
}

function appendToData(cwd, file) {
    const name = file.replace('.json', '');
    data[name] = require(cwd + file);
}

function fixture (type) {
    const cwd = path.join(__dirname, '/');

    let files = fs.readdirSync(cwd)
        .filter(isJSON)
    if (type !== 'test') {
       files = files.filter(filterTest)
    }
    files.forEach(appendToData.bind(null, cwd));
        return data
}

module.exports = fixture